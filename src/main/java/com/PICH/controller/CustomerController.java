package com.PICH.controller;

import com.PICH.model.Customer;
import com.PICH.request.CustomerRequest;
import com.PICH.response.CustomerResponse;
import com.PICH.service.CustomerService;
import org.apache.ibatis.annotations.Delete;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/customer")
public class CustomerController {

    private final CustomerService customerService;

    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    // get all customer
    @GetMapping("/get-all-customer")
    public ResponseEntity<?> getAllCustomer() {
        List<Customer> customer = customerService.getAllCustomer();
        return ResponseEntity.ok(new CustomerResponse<List<Customer>>(
                        customer,
                        "successfully",
                        true
                )
        );

    }


    //get customer by id
    @GetMapping("/get-customer-by-id/{id}")
    public ResponseEntity<?> getCustomerById(@PathVariable int id) {
        Customer customer = customerService.getCustomerById(id);
        return ResponseEntity.ok(new CustomerResponse<Customer>(
                customer,
                "Successfully",
                true
        ));

    }

    // insert into customer
    @PostMapping("/add-new-customer")
    public ResponseEntity<?> insertCustomer(@RequestBody CustomerRequest request) {
        return ResponseEntity.ok(new CustomerResponse<Customer>(
                customerService.insertCustomer(request),
                "Successfully",
                true
        ));
    }

    //update customer by id
    @PutMapping("/update-customer-by-id/{id}")
    public ResponseEntity<?> updateCustomerById(@PathVariable int id, @RequestBody CustomerRequest update) {
        return ResponseEntity.ok(new CustomerResponse<Customer>(
                customerService.updateCustomerById(id, update),
                "successfully",
                true
        ));


    }

    //delete customer by id
    @DeleteMapping("/delete-customer-by-id/{id}")
    public ResponseEntity<?> deleteCustomerById(@PathVariable int id){
        customerService.deleteCustomerById(id);
        return ResponseEntity.ok(new CustomerResponse<>(
                null,
                "successfully delete customer ",
                true
        ));

    }
}

