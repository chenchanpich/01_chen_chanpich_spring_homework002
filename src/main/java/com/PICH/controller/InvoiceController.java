package com.PICH.controller;

import com.PICH.model.Invoice;
import com.PICH.response.CustomerResponse;
import com.PICH.service.InvoiceService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/product")
public class InvoiceController {
    private final InvoiceService invoiceService;

    public InvoiceController(InvoiceService invoiceService) {
        this.invoiceService = invoiceService;
    }

    @GetMapping("/get-all-invoice")
    public ResponseEntity<?> getAllInvoice(){
        List<Invoice> invoice = invoiceService.getAllInvoice();
        return ResponseEntity.ok(new CustomerResponse<List<Invoice>>(
                invoice,
                "Successfully Get All invoice",
                true
        ));
    }

    @GetMapping("/get-all-invoice/{id}")
    public ResponseEntity<?> getInvoiceById(@PathVariable int id){
        Invoice invoice = invoiceService.getInvoiceById(id);
        return ResponseEntity.ok(new CustomerResponse<Invoice>(
                invoice,
                "Successfully Get All invoice",
                true
        ));
    }

    @DeleteMapping("/delete-invoice-by-id/{id}")
    public ResponseEntity<?> deleteInvoiceById(@PathVariable int id){
        invoiceService.deleteInvoiceById(id);
        return ResponseEntity.ok(new CustomerResponse<Invoice>(
               null,
                "Successfully Get All invoice",
                true
        ));
    }
}
