package com.PICH.controller;

import com.PICH.model.Product;
import com.PICH.request.ProductRequest;
import com.PICH.response.CustomerResponse;
import com.PICH.service.ProductService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/product")
public class ProductController {
    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }


    @GetMapping("/get-all-product")
    public ResponseEntity<?> getAllProduct(){
        List<Product> product = productService.getAllProduct();
        return ResponseEntity.ok(new CustomerResponse<List<Product>>(
                    product,
            "successfully",
            true
        ));
    }

    @GetMapping("/get-product-by-id/{id}")
    public ResponseEntity<?> getProductById(@PathVariable int id){
        Product product = productService.getProductById(id);
        return ResponseEntity.ok(new CustomerResponse<Product>(
                product,
                "successful",
                true
        ));

    }

    @PostMapping("/add-new-product")
    public ResponseEntity<?> insertProduct(@RequestBody ProductRequest productRequest){
        return ResponseEntity.ok(new CustomerResponse<Product>(
            productService.insertProduct(productRequest),
            "successfully",
            true
        ));
    }

    @PutMapping("/update-product-by-id/{id}")
    public ResponseEntity<?> updateProductById(@PathVariable int id, @RequestBody ProductRequest updateProduct){
        return ResponseEntity.ok(new CustomerResponse<Product>(
                productService.updateProductById(id,updateProduct),
                "Successfully Update Product",
                true
        ));
    }

    @DeleteMapping("/delete-product-by-id/{id}")
    public ResponseEntity<?> deleteProductById(@PathVariable int id){
        productService.deleteProductById(id);
        return ResponseEntity.ok(new CustomerResponse<>(
             null,
             "Delete this product is successful !",
                true

        ));
    }
}
