package com.PICH.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data    // equal to setter getter
@AllArgsConstructor   // constructor with parameter
@NoArgsConstructor   // default constructor

public class Customer {
    private int customer_id;
    private String customer_name;
    private String customer_address;
    private String customer_phone;

}
