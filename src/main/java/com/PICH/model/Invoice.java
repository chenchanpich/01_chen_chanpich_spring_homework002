package com.PICH.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Invoice {
    private int invoice_id;
    private Timestamp invoice_date;
    private Customer customer_id;

    private List<Product> products;

}
