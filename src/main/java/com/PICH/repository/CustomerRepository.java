package com.PICH.repository;

import com.PICH.model.Customer;
import com.PICH.request.CustomerRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface CustomerRepository  {




    @Select("""
            SELECT * FROM Customer
            """)
     List<Customer> getAllCustomer();

    @Select("""
            SELECT * FROM customer
            where customer_id = #{id};
            """)
    Customer getCustomerById(int id);

    @Select("""
            INSERT INTO customer
            VALUES (DEFAULT, #{request.customer_name},  
                             #{request.customer_address}, 
                             #{request.customer_phone})
            RETURNING *
            """)
    Customer insertCustomer(@Param("request") CustomerRequest request);
    @Select("""
            UPDATE customer
            set customer_name = #{up.customer_name},
                customer_address=#{up.customer_address},
                customer_phone=#{up.customer_phone}
            where customer_id = #{id}
            returning * ;
            """)
    Customer updateCustomerById(int id, @Param("up") CustomerRequest update);

     @Select("""
             DELETE FROM  customer
             WHERE customer_id = #{id};
             """)

    void deleteCustomerById(int id);
}