package com.PICH.repository;

import com.PICH.model.Invoice;
//import com.PICH.model.Product;
//import com.PICH.request.InvoiceRequest;
//import com.PICH.request.ProductRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface InvoiceRepository {
    @Select("""
             SELECT * FROM invoice
            """)
    @Result(property = "customer_id", column = "customer_id",
            one = @One(select = "com.PICH.repository.CustomerRepository.getCustomerById")
    )
    @Result(property = "products", column = "invoice_id",
            many = @Many(select = "com.PICH.repository.ProductRepository.getAllProductByInvoiceId")
    )
    @Result(property = "invoice_id", column = "invoice_id")

    List<Invoice> getAllInvoice();

    @Select("""
             SELECT * FROM invoice
             WHERE invoice_id =#{id}
            """)
    @Result(property = "customer_id", column = "customer_id",
            one = @One(select = "com.PICH.repository.CustomerRepository.getCustomerById")
    )
    @Result(property = "products", column = "invoice_id",
            many = @Many(select = "com.PICH.repository.ProductRepository.getAllProductByInvoiceId")
    )
    @Result(property = "invoice_id", column = "invoice_id")


    Invoice getInvoiceById(int id);


    @Delete("""
            DELETE FROM invoice
            WHERE invoice_id =#{id}   
            """)
    int deleteInvoiceById (int id);
}
