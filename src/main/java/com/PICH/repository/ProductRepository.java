package com.PICH.repository;

import com.PICH.model.Product;
import com.PICH.request.ProductRequest;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Select;

import java.util.List;
@Mapper
public interface ProductRepository {

    @Select("""
            SELECT * FROM product
            """)
    List<Product> getAllProduct();

    @Select("""
            SELECT * FROM product
            where product_id= #{id};
            """)
    Product getProductById(int id);

    @Select("""
            INSERT INTO product
            VALUES (DEFAULT, #{pro.product_name}, #{pro.product_price})
            RETURNING *
            """)

    Product insertProduct(@Param("pro") ProductRequest productRequest);

    @Select("""
            UPDATE product
            set product_name = #{update.product_name},
                product_price = #{update.product_price}
            where product_id = #{id}
            returning * ;
            """)

    Product updateProductById(int id, @Param("update")  ProductRequest updateProduct);

    @Select("""
             DELETE FROM  product
             WHERE product_id = #{id};
             """)
    void deleteProductById(int id);

    @Select("""
            SELECT product.product_id,product.product_name,product.product_price FROM product 
            INNER JOIN invoice_detail id on product.product_id = id.product_id
            where id.invoice_id = #{id}
            """)
    @Result (property = "product_id", column = "product_id")
    List<Product> getAllProductByInvoiceId(int id);


}
