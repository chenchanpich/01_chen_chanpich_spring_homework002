package com.PICH.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data    // equal to setter getter
@AllArgsConstructor   // constructor with parameter
@NoArgsConstructor   // default constructor

public class ProductRequest {
    private String product_name;
    private double product_price;
}
