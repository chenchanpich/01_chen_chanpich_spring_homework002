package com.PICH.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CustomerResponse <T>{
    private T payload;
    private String message;
    private boolean success;


}
