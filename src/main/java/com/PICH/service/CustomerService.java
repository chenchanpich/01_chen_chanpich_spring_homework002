package com.PICH.service;

import com.PICH.model.Customer;
import com.PICH.request.CustomerRequest;

import java.util.List;

public interface CustomerService {
       

       List<Customer> getAllCustomer();

       Customer getCustomerById(int id);

       Customer insertCustomer(CustomerRequest request);
       Customer updateCustomerById(int id, CustomerRequest update);

        void deleteCustomerById(int id);
}
