package com.PICH.service;

import com.PICH.model.Customer;
import com.PICH.repository.CustomerRepository;
import com.PICH.request.CustomerRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerServiceImplement implements CustomerService {

    private final CustomerRepository repository;

    public CustomerServiceImplement(CustomerRepository repository) {
        this.repository = repository;
    }


    @Override
    public List<Customer> getAllCustomer() {
        return repository.getAllCustomer();
    }

    @Override
    public Customer getCustomerById(int id) {
        return repository.getCustomerById(id);
    }

    @Override
    public Customer insertCustomer(CustomerRequest request) {
        return repository.insertCustomer(request);
    }

    @Override
    public Customer updateCustomerById(int id, CustomerRequest update) {
        return repository.updateCustomerById(id,update);
    }

    @Override
    public void deleteCustomerById(int id) {
          repository.deleteCustomerById(id);

    }


}
