package com.PICH.service;

import com.PICH.model.Invoice;

import java.util.List;


public interface InvoiceService {
    List<Invoice> getAllInvoice();

    Invoice getInvoiceById(int id);

    int deleteInvoiceById (int id);
}
