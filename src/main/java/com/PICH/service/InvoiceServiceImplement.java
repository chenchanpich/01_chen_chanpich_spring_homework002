package com.PICH.service;

import com.PICH.model.Invoice;
import com.PICH.repository.InvoiceRepository;
import com.PICH.request.InvoiceRequest;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class InvoiceServiceImplement implements InvoiceService{
    private final InvoiceRepository repository;

    public InvoiceServiceImplement(InvoiceRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Invoice> getAllInvoice() {
        return repository.getAllInvoice();
    }

    @Override
    public Invoice getInvoiceById(int id) {
        return repository.getInvoiceById(id);
    }

    @Override
    public int deleteInvoiceById(int id) {
        return repository.deleteInvoiceById(id);
    }


}
