package com.PICH.service;

import com.PICH.model.Product;
import com.PICH.request.ProductRequest;
import org.springframework.stereotype.Service;

import java.util.List;

public interface ProductService {
    List<Product> getAllProduct();


    Product getProductById(int id);

    Product insertProduct(ProductRequest productRequest);

    Product updateProductById(int id, ProductRequest updateProduct);

    void deleteProductById(int id);
}
