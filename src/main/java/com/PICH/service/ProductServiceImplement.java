package com.PICH.service;

import com.PICH.model.Product;
import com.PICH.repository.ProductRepository;
import com.PICH.request.ProductRequest;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class ProductServiceImplement implements ProductService{

    private ProductRepository productRepository;

    public ProductServiceImplement(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public List<Product> getAllProduct() {
        return productRepository.getAllProduct() ;
    }

    @Override
    public Product getProductById(int id) {
        return productRepository.getProductById(id);
    }

    @Override
    public Product insertProduct(ProductRequest productRequest) {
        return productRepository.insertProduct(productRequest);
    }

    @Override
    public Product updateProductById(int id, ProductRequest updateProduct) {
        return productRepository.updateProductById(id,updateProduct);
    }

    @Override
    public void deleteProductById(int id) {
        productRepository.deleteProductById(id);
    }

}
