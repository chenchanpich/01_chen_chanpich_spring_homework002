create table product
(
    product_id    serial
        primary key,
    product_name  varchar,
    product_price numeric(6, 2)
);

alter table product
    owner to postgres;

create table customer
(
    customer_id      serial
        primary key,
    customer_name    varchar,
    customer_address varchar,
    customer_phone   varchar
);

alter table customer
    owner to postgres;

create table invoice
(
    invoice_id   serial
        primary key,
    invoice_date date,
    customer_id  integer not null
        references customer
            on update cascade on delete cascade
);

alter table invoice
    owner to postgres;

create table invoice_detail
(
    id         serial
        primary key,
    invoice_id integer not null
        references invoice
            on update cascade on delete cascade,
    product_id integer not null
        references product
            on update cascade on delete cascade
);

alter table invoice_detail
    owner to postgres;

